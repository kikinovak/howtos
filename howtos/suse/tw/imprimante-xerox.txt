========================================
Imprimante/scanner Xerox Workcentre 6515
========================================

Dernière révision : 30 août 2022

Ce HOWTO décrit la configuration d'une imprimante/scanner Xerox Workcentre 6515
sous OpenSUSE Tumbleweed.

YaST > Matériel > Imprimante > Ajouter

Spécifier la connexion > Détecter plus : 

  - Modèle : Xerox WorkCentre 6515

  - Connexion : socket://192.168.10.246

  - Pilote : Xerox WorkCentre 6515 driverless

  - Taille de papier : A4

  - Utiliser par défaut [X]

  - Nom arbitraire : XeroxSalleDesProfs

Confirmer par OK et imprimer une page de test.


Pas de pilotes SANE pour le scanner, mais il fonctionne avec VueScan.


-- (c) Nicolas Kovacs <info@microlinux.fr>

