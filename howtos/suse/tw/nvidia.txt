======================
Carte graphique NVidia
======================

Dernière révision : 2 août 2022

Ce HOWTO décrit la configuration d'une carte graphique NVidia sous OpenSUSE
Tumbleweed.


Introduction
------------

Dans la configuration par défaut d'un poste de travail OpenSUSE, les cartes
graphiques NVidia sont gérées par le pilote libre 'nouveau' en mode KMS
("Kernel Mode Setting"), c'est-à-dire que le module en question est chargé très
tôt dans le processus de démarrage de la machine. Mes expériences avec ces
pilotes n'ont pas été concluantes, et je préfère les remplacer par les pilotes
propriétaires fournis par le fabricant.


Identifier la carte et le pilote
--------------------------------

Identifier la carte graphique :

  # lspci | grep -i vga
  01:00.0 VGA compatible controller: NVIDIA Corporation GK208B
  [GeForce GT 710] (rev a1)

Rechercher la version du pilote pour cette carte sur le site du constructeur :

  * https://www.nvidia.fr/Download/index.aspx

Identifier le paquet correspondant :

  # zypper search --details x11-video-nvidiaG0?
  ...
  S | Name                | Type    | Version         | Arch   | Repository
  --+---------------------+---------+-----------------+--------+-----------
    | x11-video-nvidiaG04 | package | 390.151-18.1    | x86_64 | nvidia
    | x11-video-nvidiaG04 | package | 390.151-18.1    | i586   | nvidia
    | x11-video-nvidiaG05 | package | 470.129.06-54.1 | x86_64 | nvidia
    | x11-video-nvidiaG06 | package | 515.57-12.1     | x86_64 | nvidia


Installation
------------

Basculer en mode console :

  # systemctl set-default multi-user.target
  # systemctl isolate multi-user.target

Le dépôt NVidia doit être configuré :

  # cat /etc/zypp/repos.d/nvidia.repo
  [nvidia]
  enabled=1
  autorefresh=0
  baseurl=http://download.nvidia.com/opensuse/tumbleweed
  priority=100
  keeppackages=1

Installer le pilote :

  # zypper install --auto-agree-with-licenses x11-video-nvidiaG05

  > Vérifier en passant si le système est à jour.

Redémarrer :

  # reboot


Configuration
-------------

Générer le fichier /etc/X11/xorg.conf :

  # nvidia-xconfig

Lancer YaST en ligne de commande :

  # yast

Configurer le chargeur de démarrage GRUB :

  - Ouvrir 'System' > 'Boot Loader' > 'Kernel Parameters'.

  - Supprimer l'option 'splash=silent'.

  - Définir la résolution optimale de l'écran dans le framebuffer : 'Graphical
    console' > 'Console resolution'.

Redémarrer et tester la configuration :

  # systemctl isolate graphical.target

Vérifier si le pilote 'nvidia' est bien utilisé :

  # grep nvidia /var/log/Xorg.0.log
  [    36.609] (II) LoadModule: "nvidia"
  [    36.609] (II) Loading /usr/lib64/xorg/modules/drivers/nvidia_drv.so
  [    36.626] (II) Module nvidia: vendor="NVIDIA Corporation"
  ...

Rebasculer en mode graphique par défaut :

  # systemctl set-default graphical.target


-- (c) Nicolas Kovacs <info@microlinux.fr>

