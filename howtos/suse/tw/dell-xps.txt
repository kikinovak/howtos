===========
Dell XPS 13
===========

Dernière révision : 3 août 2022

Ce HOWTO décrit l'installation et la configuration d'OpenSUSE Tumbleweed sur un
portable Dell XPS 13.


Configuration de l'UEFI
-----------------------

Appuyer sur [F2] pour accéder à l'UEFI au démarrage.

Restaurer la configuration par défaut de l'UEFI :

  - Overview > Load Defaults > Factory Settings.

Régler la luminosité de l'écran dans l'onglet 'Video' :

  - Brightness on AC power : 100.

  - Brightness on battery power : 100.

  - Enable EcoPower : OFF.

Onglet 'System Configuration' :

  - SATA Operation > AHCI.

  - Enable SMART Reporting > ON.

  - Enable Thunderbolt Boot Support > ON.

Onglet 'Performance' :

  - Enable Intel SpeedStep Technology > OFF.

  - Enable C-State Control > OFF.

Onglet 'Secure Boot' :

  - Enable Secure Boot > OFF.

  > Peut être activé, ça fonctionne parfaitement avec OpenSUSE.

Enregistrer la configuration et appuyer sur [F12] pour démarrer sur la clé USB.


Configuration réseau
--------------------

Le portable ne dispose pas de carte réseau filaire. Il faut donc configurer une
connexion Wi-Fi provisoire pour l'installation.

  - Paramètres réseau > Wi-Fi 6 AX-200 > Modifier.

  - Choisir le réseau > repérer le SSID dans la liste > Sélectionner.

  - Fournir le mot de passe et cliquer sur 'Suivant'.


Partitionnement
---------------

La machine est équipée d'un disque NVMe d'une capacité de 512 Go, que je
partitionne comme ceci :

  - une partition EFI de 200 Mo, étiquetée EFI.

  - une partition /boot de 500 Mo, formatée en ext2 et étiquetée boot.

  - une partition swap de 16 Go, étiquetée swap.

  - une partition / de 460 Go, formatée en ext4 et étiquetée root.

  # lsblk
  NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
  nvme0n1     259:0    0 476,9G  0 disk
  ├─nvme0n1p1 259:1    0   200M  0 part /boot/efi
  ├─nvme0n1p2 259:2    0   500M  0 part /boot
  ├─nvme0n1p3 259:3    0    16G  0 part [SWAP]
  └─nvme0n1p4 259:4    0 460,3G  0 part /


Résolution de l'affichage
-------------------------

La résolution de l'affichage est fixée à 1920 x 1080 pixels.

Obtenir un bureau plus lisible :

  - Paramètres KDE > Affichage et écran > Échelle globale : 137,5x.

  - Augmenter la hauteur du tableau de bord à 48 pixels.


-- (c) Nicolas Kovacs <info@microlinux.fr>

