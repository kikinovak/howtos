===========
Mise à jour
===========

Dernière révision : 2 août 2022

Ce HOWTO décrit la mise à jour "à chaud" d'une installation OpenSUSE Leap vers
Tumbleweed.


Méthode testée avec succès sur :

  - OpenSUSE Leap 15.2

  - OpenSUSE Leap 15.3

  - OpenSUSE Leap 15.4

  > Ce n'est pas la peine d'effectuer des mises à jour intermédiaires.


Quitter l'environnment graphique :

  # systemctl set-default multi-user.target
  # systemctl isolate multi-user.target

Désactiver DeltaRPM :

  # /etc/zypp/zypp.conf
  ...
  download.use_deltarpm = false
  ...

Installer Git :

  # zypper install --no-recommends git

Récupérer le script de mise à jour :

  # git clone https://gitlab.com/kikinovak/opensuse

Lancer la mise à jour :

  # cd opensuse
  # ./upgrade.sh

Redémarrer au terme de la mise à jour :

  # reboot

Tester et lancer l'environnement graphique :

  # systemctl isolate graphical.target
  # systemctl set-default graphical.target


-- (c) Nicolas Kovacs <info@microlinux.fr>

