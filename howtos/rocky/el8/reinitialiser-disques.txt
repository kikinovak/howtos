=========================
Réinitialiser les disques
=========================

Dernière révision : 3 août 2022

Ce HOWTO décrit la réinitialisation d'un ou plusieurs disques durs sous Rocky
Linux 8.


Démarrer la console de secours.

Afficher les disques et les partitions :

  # lsblk

Désactiver les assemblages RAID provenant d'une installation antérieure :

  # mdadm --stop --scan

Effacer les métadonnées RAID persistantes sur les partitions :

  # mdadm --zero-superblock /dev/sda1
  # mdadm --zero-superblock /dev/sda2
  # mdadm --zero-superblock /dev/sda3
  # mdadm --zero-superblock /dev/sdb1
  # mdadm --zero-superblock /dev/sdb2
  # mdadm --zero-superblock /dev/sdb3
  ...

Supprimer les tables de partitions :

  # sgdisk --zap-all /dev/sda
  # sgdisk --zap-all /dev/sdb
  ...

Effacer les signatures des systèmes de fichiers :

  # wipefs --all /dev/sda
  # wipefs --all /dev/sdb
  ...

Détruire les données d'un disque conformément au standard 5220-22M US Dept. of
Defense :

  # shred -vzn 8 /dev/sda


-- (c) Nicolas Kovacs <info@microlinux.fr>


