================
Console Scaleway
================

Dernière révision : 3 août 2022

Ce HOWTO décrit le lancement de la console de secours Scaleway.


Sur un serveur dédié Scaleway, on pourra démarrer le système de secours :

  - Se connecter à la console Online : https://console.online.net 

  - Ouvrir le menu 'Serveur' > 'Liste des serveurs'.

  - Sélectionner la machine > 'Administrer' > 'Secours'.

  - Sélectionner un système d'exploitation > Ubuntu 18.04.

  - Cliquer sur 'Lancer le système de secours sur votre Dedibox'.

    Attendre cinq minutes.

Ouvrir une connexion SSH avec les paramètres affichés :

  $ ssh microlinux@51.158.146.161

Devenir root :

  $ sudo -s

Une fois que l'intervention est terminée, cliquer sur 'Repasser en mode
normal'.


-- (c) Nicolas Kovacs <info@microlinux.fr>

