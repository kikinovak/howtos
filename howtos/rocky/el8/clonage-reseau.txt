==============================
Cloner un disque via le réseau
==============================

Dernière révision : 3 août 2022

Ce HOWTO décrit le clonage "octet par octet" d'un disque dur sur une machine
distante.


Écrire des zéros sur la partie non utilisée du disque pour réduire la taille de
l'image :

  # dd if=/dev/zero of=/0bits bs=20M; rm -f /0bits

  > Sur un système Windows 10, c’est l’utilitaire nullfile.exe qui permet de
  > réduire l’image du disque : 

    * http://www.feyrer.de/g4u/nullfile-1.01_64bit.exe

Sauvegarder une machine locale depuis la console de secours :

  # dd if=/dev/sdb status=progress | gzip --fast - | \
    ssh microlinux@nestor dd of=rocky-8-server-image.gz

  - status=progress affiche la progression pour dd.

  - --fast spécifie un algorithme de compression plus rapide pour gzip.

Restaurer cette machine :

  # ssh microlinux@nestor dd if=rocky-8-server-image.gz | \
    gunzip --fast - | dd of=/dev/sdb status=progress

Sauvegarder un serveur dédié depuis une session de secours :

  # dd if=/dev/sda status=progress | gzip --fast - | \
    ssh microlinux@backup.microlinux.fr dd of=rocky-8-server-image.gz

Restaurer cette machine :

  # ssh microlinux@backup.microlinux.fr dd if=rocky-8-server-image.gz | \
    gunzip --fast - | dd of=/dev/sda status=progress


-- (c) Nicolas Kovacs <info@microlinux.fr>

